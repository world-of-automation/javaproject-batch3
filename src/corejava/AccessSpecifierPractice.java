package corejava;

public class AccessSpecifierPractice {


    static String name = "Ash";

    public static void main(String[] args) {

        // public  -- can be accessed from anywhere in the project
        // private  -- can be accessed from the same class only
        // protected -- can be accessed from the same package and not in the inner class/ subclass
        // default  -- can be accessed from the same package and inner class in same package

        dosomething();

    }


    static void doEverything() {
        System.out.println("something");
        String name = "Ash";
    }

    private static void dosomething() {
        System.out.println("something");
    }

    protected static void doNothing() {
        System.out.println("something");
    }

    //default
    static void imboredtoDoAnything() {
        System.out.println("bored");
    }


}
