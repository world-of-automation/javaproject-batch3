package corejava;

public class Laptop {

    int model = 2020;

    public void outerClassMethod() {
        Memory memory = new Memory();
        System.out.println(memory.ram);
    }

    private class Memory {
        int ram = 1024;
    }
}
