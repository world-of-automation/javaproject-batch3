package corejava;


import java.util.Scanner;

public class ScannerTest {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("please insert your name : ");
        String name = scanner.next();
        System.out.println("please insert your age : ");
        int age = scanner.nextInt();
        System.out.println("please insert your gender : ");
        String gender = scanner.next();
        System.out.println("you are a guitarist");
        boolean guitarist = scanner.nextBoolean();

        //InputMismatchException

        System.out.println("your all details are : " + name + " " + age + " " + gender + " " + guitarist);

    }
}

