package corejava;

import corejava.objects.Guitar;

public class VariablesPractice {

    // global variables --> has access specifier
    public static String laptop = "mac";

    public static void main(String[] args) {
        myMobilesBrandName();
        System.out.println(laptop + " from main method");
    }

    public static void myMobilesBrandName() {
        //local variables --> doesn't have access specifier
        String mobileBrand = "Samsung";
        System.out.println(mobileBrand);
        System.out.println(laptop + " from void method");

        Guitar guitar = new Guitar("Jackson");
        guitar.printBrandOfGuitar();

    }

}
