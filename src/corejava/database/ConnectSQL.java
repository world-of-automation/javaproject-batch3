package corejava.database;

import corejava.inputoutput.PropertiesFileUtils;

import java.sql.*;
import java.util.Properties;

public class ConnectSQL {
    private static final Properties properties;

    static {
        properties = PropertiesFileUtils.readPropertiesFile("src/corejava/database/config.properties");
    }

    public static void closeConnection(Connection connection, Statement statement, ResultSet resultSet) {
        //resultSet.close();
        //statement.close();
        //connection.close();
        try {
            resultSet.close();
        } catch (SQLException ee) {
            ee.printStackTrace();
        }
        try {
            statement.close();
        } catch (SQLException ee) {
            ee.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException ee) {
            ee.printStackTrace();
        }
        System.out.println("All the connections, statements and resultSet are closed");
    }


    public static Connection getConnection(String databaseName) throws SQLException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ee) {
            System.out.println("please correct the driver name");
            ee.printStackTrace();
        }
        String url = "jdbc:mysql://localhost:3306/" + databaseName + "?serverTimezone=UTC";
        String userName = properties.getProperty("userName");
        String password = properties.getProperty("password");
        Connection connection = DriverManager.getConnection(url, userName, password);
        return connection;
    }

    public static Statement getStatement(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        return statement;
    }

    public static ResultSet getResultSet(Statement statement, String query) throws SQLException {
        ResultSet table = statement.executeQuery(query);
        return table;
    }

}
