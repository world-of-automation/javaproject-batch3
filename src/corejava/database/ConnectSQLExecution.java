package corejava.database;

import corejava.inputoutput.PropertiesFileUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class ConnectSQLExecution {

    // templete for jdbc connections
    public static void main(String[] args) throws SQLException {
        Properties properties = PropertiesFileUtils.readPropertiesFile("src/corejava/database/config.properties");

        // required db name and query
        String databaseName = properties.getProperty("databaseName");
        String selectQuery = properties.getProperty("selectQuery");

        // 3 objects
        Connection connection = ConnectSQL.getConnection(databaseName);
        Statement statement = ConnectSQL.getStatement(connection);
        ResultSet table = ConnectSQL.getResultSet(statement, selectQuery);

        //loop
        while (table.next()) {
            int id = table.getInt("id");
            String name = table.getString("name");
            int salary = table.getInt("salary");
            String location = table.getString("location");
            System.out.println(id + " " + name + " " + salary + " " + location);
        }

        //clear connections
        ConnectSQL.closeConnection(connection, statement, table);
    }
}
