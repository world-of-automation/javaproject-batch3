package corejava.database;

import java.sql.*;

public class ConnectSQLTest1 {

    public static void main(String[] args) throws SQLException {
        Connection connection = null;
        Statement statement = null;
        ResultSet table = null;
        try {
            String userName = "root";
            String password = "root1234";
            String databaseName = "batch3";
            //jdbc:mysql --driver of the database
            //localhost -- host name
            //3306      -- port number
            //?serverTimezone=UTC -- only if exception occured as below
            //The server time zone value 'EDT'
            //?useSSL=false" -- Only if you get SSL error

            String url = "jdbc:mysql://localhost:3306/" + databaseName + "?serverTimezone=UTC";
            String selectQuery = "select * from employee;";

            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            connection = DriverManager.getConnection(url, userName, password);
            statement = connection.createStatement();
            table = statement.executeQuery(selectQuery);

            while (table.next()) {
                int id = table.getInt("id");
                String name = table.getString("name");
                int salary = table.getInt("salary");
                String location = table.getString("location");
                System.out.println(id + " " + name + " " + salary + " " + location);
            }

        } catch (Exception ee) {

        } finally {
            table.close();
            statement.close();
            connection.close();
        }


    }

}
