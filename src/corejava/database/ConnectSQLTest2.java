package corejava.database;

import corejava.inputoutput.PropertiesFileUtils;

import java.sql.*;

public class ConnectSQLTest2 {

    public static void main(String[] args) throws SQLException {

        String userName = PropertiesFileUtils.getPropertyFromFile("src/corejava/database/config.properties", "userName");

        String password = PropertiesFileUtils.getPropertyFromFile("src/corejava/database/config.properties", "password");
        String databaseName = PropertiesFileUtils.getPropertyFromFile("src/corejava/database/config.properties", "databaseName");
        String url = "jdbc:mysql://localhost:3306/" + databaseName + "?serverTimezone=UTC";
        String selectQuery = PropertiesFileUtils.getPropertyFromFile("src/corejava/database/config.properties", "selectQuery");

        ResultSet table = getResultSet(url, userName, password, selectQuery);

        while (table.next()) {
            int id = table.getInt("id");
            String name = table.getString("name");
            int salary = table.getInt("salary");
            String location = table.getString("location");
            System.out.println(id + " " + name + " " + salary + " " + location);
        }

    }

    public static ResultSet getResultSet(String url, String userName, String password, String query) throws SQLException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ee) {
            System.out.println("please correct the driver name");
            ee.printStackTrace();
        }
        Connection connection = DriverManager.getConnection(url, userName, password);
        Statement statement = connection.createStatement();
        ResultSet table = statement.executeQuery(query);
        return table;
    }


}
