package corejava.database;

import corejava.inputoutput.PropertiesFileUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

public class PracticeSQLArrayList {

    public static void main(String[] args) throws SQLException {
        Properties properties = PropertiesFileUtils.readPropertiesFile("src/corejava/database/config.properties");
        String databaseName = properties.getProperty("databaseName");
        String selectQuery = properties.getProperty("selectQuery");

        Connection connection = ConnectSQL.getConnection(databaseName);
        Statement statement = ConnectSQL.getStatement(connection);
        ResultSet table = ConnectSQL.getResultSet(statement, selectQuery);

        ArrayList<Integer> listOfId = new ArrayList<>();
        ArrayList<String> listOfName = new ArrayList<>();
        ArrayList<Integer> listOfSalary = new ArrayList<>();
        ArrayList<String> listOfLocation = new ArrayList<>();

        while (table.next()) {
            int id = table.getInt("id");
            listOfId.add(id);
            String name = table.getString("name");
            listOfName.add(name);

            listOfSalary.add(table.getInt("salary"));
            listOfLocation.add(table.getString("location"));
        }

        System.out.println(listOfId);
        System.out.println(listOfName);
        System.out.println(listOfName.get(2));

        ConnectSQL.closeConnection(connection, statement, table);
    }
}
