package corejava.database;

import corejava.inputoutput.PropertiesFileUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class PracticeSQLMap {

    public static void main(String[] args) throws SQLException {
        Properties properties = PropertiesFileUtils.readPropertiesFile("src/corejava/database/config.properties");
        String databaseName = properties.getProperty("databaseName");
        String selectQuery = properties.getProperty("selectQuery");
        Connection connection = ConnectSQL.getConnection(databaseName);
        Statement statement = ConnectSQL.getStatement(connection);
        ResultSet table = ConnectSQL.getResultSet(statement, selectQuery);

        //home work
        // store name and id in the hashmap and print using the hashmap

        while (table.next()) {
            int id = table.getInt("id");
            String name = table.getString("name");

        }

        ConnectSQL.closeConnection(connection, statement, table);
    }
}
