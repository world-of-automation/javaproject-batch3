package corejava.datastructures;

import java.util.ArrayList;
import java.util.Iterator;

public class ArrayListPractice {

    public static ArrayList<String> arrayListOfDsata() {
        ArrayList<String> countryList = new ArrayList<>();
        countryList.add("USA");
        countryList.add("Canada");
        countryList.add("Bangladesh");
        return countryList;
    }

    public static void main(String[] args) {
        ArrayList<String> sata = arrayListOfDsata();
        System.out.println(sata);


        System.out.println("***************");
        ArrayList<String> countryList = new ArrayList<>();
        countryList.add("USA");
        countryList.add("Canada");
        countryList.add("Bangladesh");

        System.out.println(countryList);
        System.out.println(countryList.size());

        countryList.add("Poland");
        System.out.println(countryList);
        System.out.println(countryList.size());

        System.out.println(countryList.get(0));
        System.out.println(countryList.get(3));


        // print all data using a for loop

        System.out.println("********");
        for (int i = 0; i < countryList.size(); i++) {
            System.out.println(countryList.get(i));
        }

        System.out.println("********");

        countryList.remove(3);
        System.out.println(countryList);

        System.out.println("********");
        System.out.println(countryList.isEmpty());
        countryList.clear();
        System.out.println(countryList);
        System.out.println(countryList.isEmpty());

        // boolean & double etc should needs to use the Class Name (Boolean)
        ArrayList<Integer> zipCodes = new ArrayList();
        zipCodes.add(11213);
        System.out.println(zipCodes);

        ArrayList<Object> stateList = new ArrayList();
        stateList.add("NY");
        stateList.add(11);
        stateList.add(true);

        System.out.println(stateList);

        //using Iterator
        Iterator iterator = stateList.iterator();

        while (iterator.hasNext()) { // HASNEXT WILL BE FALSE WHEN THERE'S NO MORE DATA LEFT
            System.out.println(iterator.next()); // NEXT WILL RETURN ME THE VALUE FROM THE ITERATOR
        }


        ArrayList<String> datas = new ArrayList<>();
        datas.add("laptop");
        datas.add("laptop");
        datas.add(null);
        datas.add(null);
        System.out.println(datas);
        System.out.println(datas.get(2));
        System.out.println(datas.size());
    }
}
