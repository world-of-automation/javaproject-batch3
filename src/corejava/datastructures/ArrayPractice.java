package corejava.datastructures;

public class ArrayPractice {


    public static void main(String[] args) {
        String name = "Zann,Ash";

        String[] names = {"Zann", "Ash", "Sam", "Shakir", "Tanbir"};
        int[] numbers = {1, 2, 5, 45};

        System.out.println(names);
        System.out.println(names.length);
        System.out.println(numbers.length);

        System.out.println(names[1]);
        System.out.println("*******Before For Loop");

        for (int i = 0; i < 5; i++) {
            System.out.println(names[i]);
        }

        System.out.println("*******After For Loop");
        Object[] datas = {1, 2, "Sam", true, 190f, "Shakir", "Zann", 13413};

        for (int i = 0; i < datas.length; i++) {
            System.out.println(datas[i]);
        }

        System.out.println("*******************");

        // dynamic array
        // Object[] datas = {1, 2, "Sam", true, 190f, "Shakir", "Zann", 13413};
        String[] cars = new String[3];
        int[] num = new int[3];
        cars[0] = "honda";
        cars[1] = "audi";
        cars[2] = "bmw";

        try {
            cars[4] = "alpha romeo";
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("index was more than expected");
        }

        cars[2] = "benz";

        for (int i = 0; i < cars.length; i++) {
            System.out.println(cars[i]);
        }

        //ArrayIndexOutOfBoundsException

        String[] nextGenCars = cars.clone();
        System.out.println(nextGenCars[2]);
        System.out.println(nextGenCars.length);

    }

}
