package corejava.datastructures;

public class ArrayPractice2 {

    public static void main(String[] args) {
        // declare a int array with index of 10
        // where it will store 0 for every even number
        // and it will store 1 for every odd number

        int[] numbers = new int[1000];

        for (int i = 0; i < numbers.length; i++) {
            if (i % 2 == 0) {
                numbers[i] = 0;
            } else {
                numbers[i] = 1;
            }
        }

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

    }
}
