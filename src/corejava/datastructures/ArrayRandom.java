package corejava.datastructures;

import java.util.Random;

public class ArrayRandom {
    public static void main(String[] args) {
        Random random = new Random();
        //int randomInt = random.nextInt(100);
        //System.out.println(randomInt);

        // write a java program which will store 5 random data into a array

        int[] randomArray = new int[5];

        for (int i = 0; i < randomArray.length; i++) {
            if (i > 1) {
                randomArray[i] = random.nextInt(200);
            }
        }

        for (int i = 0; i < randomArray.length; i++) {
            System.out.println(randomArray[i]);
        }


    }
}
