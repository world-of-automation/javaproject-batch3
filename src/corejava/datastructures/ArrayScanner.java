package corejava.datastructures;

import java.util.Scanner;

public class ArrayScanner {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("please insert 5 of your fav food : ");
        String[] favFoods = new String[5];

        for (int i = 0; i < 5; i++) {
            favFoods[i] = scanner.next();
        }

        System.out.println("your fav foods are : ");
        for (int i = 0; i < favFoods.length; i++) {
            System.out.println(favFoods[i]);
        }

    }
}

