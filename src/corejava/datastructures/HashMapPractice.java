package corejava.datastructures;

import java.util.ArrayList;
import java.util.HashMap;

public class HashMapPractice {

    public static void main(String[] args) {
        HashMap<Integer, String> zipCodesOfNeighbourhoods = new HashMap<>();
        zipCodesOfNeighbourhoods.put(11374, "Rego Park");
        zipCodesOfNeighbourhoods.put(11763, "Medford");

        System.out.println(zipCodesOfNeighbourhoods.get(11374));

        System.out.println(zipCodesOfNeighbourhoods);
        System.out.println(zipCodesOfNeighbourhoods.size());
        System.out.println(zipCodesOfNeighbourhoods.containsKey(11374));
        System.out.println(zipCodesOfNeighbourhoods.containsKey(11375));
        System.out.println(zipCodesOfNeighbourhoods.containsValue("Medford"));
        System.out.println(zipCodesOfNeighbourhoods.containsValue("Jamaica"));

        HashMap<String, String> mapOfstates = new HashMap<>();
        mapOfstates.put("New York", "NY");
        mapOfstates.put("New Jersey", "NJ");
        mapOfstates.put("Virginia", "VA");

        System.out.println(mapOfstates.get("New York"));
        System.out.println(mapOfstates.get(0));

        System.out.println(mapOfstates);

        ArrayList<String> keys = new ArrayList<>(mapOfstates.keySet());
        System.out.println(keys);

        System.out.println(mapOfstates.get("New York"));
        System.out.println(keys.get(0));
        System.out.println(mapOfstates.get(keys.get(0)));

        for (int i = 0; i < mapOfstates.size(); i++) {
            System.out.println(mapOfstates.get(keys.get(i)));
        }


        HashMap<Integer, String> datas = new HashMap<>();
        datas.put(100, "Pen");
        datas.put(22, "Paper");
        datas.put(100, "Pencil");
        datas.put(12, "Pencil");
        datas.put(null, "Chair");
        datas.put(null, "Table");
        datas.put(33, null);
        datas.put(34, null);
        System.out.println(datas.size());
        System.out.println(datas.get(12));
        System.out.println(datas.get(100));
        System.out.println(datas.get(null));
        System.out.println(datas.get(33));
        System.out.println(datas.get(34));

    }


}
