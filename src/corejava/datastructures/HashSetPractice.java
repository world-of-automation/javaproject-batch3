package corejava.datastructures;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class HashSetPractice {

    public static void main(String[] args) {

        HashSet<String> cities = new HashSet<>();
        cities.add("rego park");
        cities.add("jamaica");
        cities.add("rego park");
        cities.add("medford");

        System.out.println(cities.size());
        System.out.println(cities);

        Iterator iterator = cities.iterator();

       /* while (iterator.hasNext()) {
           String dataFromIt = iterator.next();
        }*/

        ArrayList<String> citiesAsList = new ArrayList<>();
        while (iterator.hasNext()) {
            citiesAsList.add(iterator.next().toString());
        }
        System.out.println(citiesAsList.get(1));
    }
}
