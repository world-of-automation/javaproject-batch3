package corejava.datastructures;

import java.util.Hashtable;

public class HashTablePractice {

    //hashmap takes null key/value --> hashtable doesn't
    //hashmap isn't thread safe & not synchronized ---> hashtable is
    //hashmap is fast-->hashtable slow

    public static void main(String[] args) {

        Hashtable<Integer, String> tableOfData = new Hashtable<>();
        tableOfData.put(100, "ENG101");
        tableOfData.put(109, "BUS101");
        tableOfData.put(190, "LEC101");
        //tableOfData.put(133,null);
        //tableOfData.put(null,"ENV234");
        System.out.println(tableOfData);

    }
}
