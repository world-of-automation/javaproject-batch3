package corejava.datastructures;

import java.util.Stack;

public class Test {

    public static void main(String[] args) {

        Stack<String> data = new Stack<>();

        data.push("Zann");
        data.push("Shakir");


        System.out.println(data.peek());
        System.out.println(data.pop());
    }
}
