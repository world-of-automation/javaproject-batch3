package corejava.enumPractice;


import static corejava.enumPractice.Days.TUESDAY;

public class Execution {

    public static void main(String[] args) {
        whatToDO(TUESDAY);
    }

    public static void whatToDO(Days day) {
        switch (day) {
            case FRIDAY:
                System.out.println("i have to go to mosque");
                break;
            case SATURDAY:
                System.out.println("i have to play soccer");
                break;
            case SUNDAY:
            case MONDAY:
                System.out.println("i have to play guitar");
                break;
            default:
                System.out.println("i have to go to work");
                break;
        }
    }
}
