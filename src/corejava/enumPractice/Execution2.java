package corejava.enumPractice;

public class Execution2 {

    public static void main(String[] args) {
        weatherOf(Months.APRIL);
    }

    public static void weatherOf(Months months) {
        switch (months) {
            case JANUARY:
                System.out.println("Coldddddd");
                break;
            case FEBRUARY:
                System.out.println("Snow");
                break;
            case APRIL:
            case MARCH:
                System.out.println("more cold");
                break;
            default:
                System.out.println("chilling weather");
                break;
        }
    }
}
