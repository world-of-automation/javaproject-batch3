package corejava.inputoutput;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesFileUtils {

    public static void main(String[] args) {
        Properties properties = readPropertiesFile("src/corejava/database/config.properties");
        String pass = properties.getProperty("password");
        System.out.println(pass);

        //second
        String password = getPropertyFromFile("src/corejava/database/config.properties", "password");
        System.out.println(password);
    }

    public static Properties readPropertiesFile(String filePath) {
        Properties properties = new Properties();
        try {
            InputStream inputStream = new FileInputStream(filePath);
            properties.load(inputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }

    //homework
    //what this method is doing
    //how it can help our coding
    public static String getPropertyFromFile(String filePath, String key) {
        Properties properties = readPropertiesFile(filePath);
        return properties.getProperty(key);
    }

}
