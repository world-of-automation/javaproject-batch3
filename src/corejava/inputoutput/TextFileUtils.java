package corejava.inputoutput;

import java.io.BufferedReader;
import java.io.FileReader;

public class TextFileUtils {

    public static void main(String[] args) {
        String filePath = "src/corejava/inputoutput/Test.txt";

        String textFromFile = null;
        try {
            textFromFile = readTextFile(filePath);
        } catch (Exception e) {

            //read second file
        }
        System.out.println(textFromFile);

        /*String a = "Zan";
        String b = "Sam";
        String c= a+"\n"+b;
        System.out.println(c);*/
    }


    public static String readTextFile(String filePath) throws Exception {
        String finalText = "";
        String tempContainer;


        FileReader fileReader = new FileReader(filePath);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        while ((tempContainer = bufferedReader.readLine()) != null) {
            finalText = finalText + "\n" + tempContainer;
        }

        bufferedReader.close();
        return finalText;
    }


}
