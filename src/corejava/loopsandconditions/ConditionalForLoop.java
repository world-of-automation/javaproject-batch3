package corejava.loopsandconditions;

public class ConditionalForLoop {

    public static void main(String[] args) {
        for (int i = 0; i < 11; i++) {
            if (i < 5) {
                System.out.println("NY " + i);
            } else if (i == 6) {
                System.out.println("NJ " + i);
            } else if (i == 10) {
                System.out.println("FL " + i);
            } else if (i != 8) {
                System.out.println("VA " + i);
            } else {
                System.out.println("DC " + i);
            }
        }
    }
}
