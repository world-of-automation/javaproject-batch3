package corejava.loopsandconditions;

public class DoWhileLoop {

    public static void main(String[] args) {
        int i = 0;//start point
        do {
            System.out.println("Coronavirus " + i);
            i++;// increment/decrement
        }
        while (i < 5); //end poin
    }

}
