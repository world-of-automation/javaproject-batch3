package corejava.loopsandconditions;

public class ForLoopPractice {


    public static void main(String[] args) {

        //for--keyword(--for conditions--){--body starts
        //}--body ends

        //(--for conditions--)
        //(starting_point;end_point;increment++/decrement--)
        for (int i = 0; i < 5; i++) {
            System.out.println("Java " + i);
        }

        for (int i = 0; i < 5; i++) {
            System.out.println("Selenium " + i);
        }

        System.out.println("*************");

        // print java 5 times
        // and for each time of java print selenium 2 times

        for (int i = 0; i < 5; i++) {
            System.out.println("Java " + i);
            System.out.println("selenium");
            System.out.println("selenium");
        }

        System.out.println("*************");


    }

}
