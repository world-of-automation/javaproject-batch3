package corejava.loopsandconditions;

public class IfElse {

    // create a static String method which will take a int,
    // and if the int is more than 10 it will return "successful"
    // else it will return "unsuccessful"

    public static String getResult(int value) {
        if (value > 10) {
            return "successful";
        } else {
            return "unsuccessful";
        }
    }

    // create a static String method which will take  String,
    //  if the String is monday it will return "i'll go to work"
    // else if the String is tuesday it will return "i'll go to gym"
    // else if the String is wednesday or thursday it will return "i'll go to soccer"
    // else for the other days or whatever it will return "i'll just chill on my bed"

    // == --> equals sign
    // || --> or sign
    // && --> and sign
    // != --> not equals sign

    public static String getResultOfTheDays(String dayName) {
        if (dayName.equalsIgnoreCase("monday")) {
            return "i'll go to work";
        } else if (dayName.equalsIgnoreCase("tuesday")) {
            return "i'll go to gym";
        } else if (dayName.equalsIgnoreCase("wednesday") || dayName.equalsIgnoreCase("thursday")) {
            return "i'll go to soccer";
        } else {
            return "i'll just chill on my bed";
        }
    }


    // create a static String method which will take a int,
    // if the int is more than 10 and less than 15 return "new york"
    // and else if the int is not equals to 2 return "new jersey"
    // else return "virginia"

    public static String getState(int value) {
        if (value > 10 && value < 15) {
            return "NY";
        } else if (value != 2) {
            return "NJ";
        } else {
            return "VA";
        }
    }

    public static void main(String[] args) {
        String stateName = getState(2);
        System.out.println(stateName);


        String result = getResult(9);
        System.out.println(result);

        String resultOfTheDay = getResultOfTheDays("friday");
        System.out.println(resultOfTheDay);


        //if-->keyword(___conditions___){-->if block starts
        //}--if block ends else--> keyword
        //{-->else block starts
        //}-->else block ends
        int number = 11;
        if (number == 10) {
            System.out.println("i'm happy");
        } else {
            System.out.println("I'm not happy ");
        }
    }
}
