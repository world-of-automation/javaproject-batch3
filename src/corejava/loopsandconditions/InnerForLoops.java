package corejava.loopsandconditions;

public class InnerForLoops {

    public static void main(String[] args) {

        for (int i = 0; i < 5; i++) {
            System.out.println("Java");
            for (int j = 0; j < 3; j++) {
                System.out.println("Selenium");
            }
        }

    }
}
