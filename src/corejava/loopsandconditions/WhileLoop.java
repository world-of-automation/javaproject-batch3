package corejava.loopsandconditions;

public class WhileLoop {

    public static void main(String[] args) {

        // for (int i = 0; i < 5; i++) {

        //starting point
        int x = 0;

        //while-->keyword (end point)
        while (x < 5) {//body starts
            System.out.println("My name is " + x);
            x++;//---> increment/decrement
        }//body ends

    }
}
