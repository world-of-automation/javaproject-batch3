package corejava.methodspractice;

public class Methods2 {


    public static void main(String[] args) {
        myStudyRoom();
        mySchedule();
    }

    public static void myStudyRoom() {
        System.out.println("this is the room i sleep");
    }

    public static void mySchedule() {
        System.out.println("monday i'm off");
        System.out.println("sunday i'm on vacation");
    }


}



