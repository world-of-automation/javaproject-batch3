package corejava.methodspractice;

public class Methods3 {


    // create a class
    // create a main method -- psvm
    // create another method which print today's date
    // call the method that you created in the main method

    public static void main(String[] args) {
        printTodaysDate();
        String nameFromMyMethod = getMyName();
        System.out.println(nameFromMyMethod);
    }


    // void methods
    public static void printTodaysDate() {
        System.out.println("3/4/19");
    }


    //return type method
    public static String getMyName() {
        return "Zann";
    }


}
