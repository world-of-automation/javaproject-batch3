package corejava.methodspractice;

public class Methods4 {

    public static void main(String[] args) {
        goDrinkCoffee();
        String myCoffeeThatSamBrought = getMeACoffee();
        System.out.println(myCoffeeThatSamBrought);

        goDrinkCoffee();
    }


    // void methods
    public static void goDrinkCoffee() {
        System.out.println("sam drank coffee");
    }


    //return type method
    public static String getMeACoffee() {
        return "Coffee from starbucks";
    }

}
