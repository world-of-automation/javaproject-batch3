package corejava.methodspractice;

public class Methods6 {

    public static void main(String[] args) {
        calculator(10, 3463);
        calculator(10, 12, "dfdf", true, 10f);
        calculator();
    }

    // (method params){method body}
    public static void calculator(int a, int b) {
        int c = a + b;
        System.out.println(c);
    }

    public static void calculator() {
        int a = 10;
        int b = 22;
        int c = a + b;
        System.out.println(c);
    }

    public static void calculator(int a, int b, String c, boolean flag, float num) {
        System.out.println();
    }

}
