package corejava.methodspractice;

public class MethodsTest {

    int data1;
    int data2;

    public MethodsTest(int data) {
        this.data1 = data;
    }

    public MethodsTest(int data1, int data2) {
        this.data1 = data1;
        this.data2 = data2;
    }

    public static void main(String[] args) {
        MethodsTest methodsTest = new MethodsTest(10);
        methodsTest.calculator(10, methodsTest.data1);
        methodsTest.calculator(10, 12, 11);
        methodsTest.calculator();
    }

    public void calculator(int a, int b) {
        int c = a + b;
        System.out.println(c);
    }

    public void calculator() {
        int a = 10;
        int b = 22;
        int c = a + b;
        System.out.println(c);
    }

    public int calculator(int a, int b, int c) {
        int d = a + b + c;
        return d;
    }

}
