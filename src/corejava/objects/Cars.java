package corejava.objects;

public class Cars {

    public String brand = "honda";

    //default constructor
    // name of the class name(){}
    public Cars() {

    }


    public static void main(String[] args) {
        // object of the class
        // class refVar =  new ConstructorName ;
        // Cars refVar = new Cars(); // --- refVar is knows as the object of the class
        Cars cars = new Cars();
        cars.printColorOfMyCar();
        printYearOfMyCar();
        System.out.println(cars.brand);
    }

    public static void printYearOfMyCar() {
        System.out.println(2018);
    }

    public void printColorOfMyCar() {
        System.out.println("blue");
    }


}
