package corejava.objects;

public class College {

    // create a default constructor
    // create a static-void method and call into the main method
    // create a non-static-void method and call into the main method
    // create a static-return type of String method and call into the main method
    // create a non-static-return type of int method and call into the main method

    public static void printCollegeName() {
        System.out.println("DCC");
    }

    public static String getCollegeTeacher() {
        return "Teacher Name";
    }

    public String getCollegePrincipal() {
        return "principal";
    }

    public void printCollegeLocation() {
        System.out.println("Mirpur");
    }


}
