package corejava.objects;

public class CollegeExecution {

    public static void main(String[] args) {

        // non static method to call we need the obj (same class or diff class)
        // static methods we need to call via
        //  -   className.methodName() if in diff class
        //  -   methodName() if in the same class

        College.printCollegeName();
        String teacher = College.getCollegeTeacher();
        System.out.println(teacher);


        College college = new College();
        college.printCollegeLocation();

        String sdjfnwkjfnlwf = college.getCollegePrincipal();
        System.out.println(sdjfnwkjfnlwf);
        getCollegeTeacher();
    }

    public static String getCollegeTeacher() {
        return "Teacher Name";
    }

}
