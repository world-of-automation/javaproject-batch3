package corejava.objects;

public class Guitar {

    public int stringsOnTheGuitar;
    public String brandName;

    // default constructor
    public Guitar() {

    }

    // parameterized constructor
    public Guitar(int stringsOnTheGuitar) {
        System.out.println("single param constructor called");
        this.stringsOnTheGuitar = stringsOnTheGuitar;
    }

    public Guitar(String brandName) {
        this.brandName = brandName;
    }


    public Guitar(int stringsOnTheGuitar, String brandName) {
        System.out.println("two param constructor called");
        this.brandName = brandName;
        this.stringsOnTheGuitar = stringsOnTheGuitar;
    }

    public void printStringOfGuitar() {
        System.out.println(stringsOnTheGuitar);
    }

    public void printBrandOfGuitar() {
        System.out.println(brandName);
    }

    public void printAll() {
        System.out.println(stringsOnTheGuitar);
        System.out.println(brandName);
    }


}
