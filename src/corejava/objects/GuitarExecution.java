package corejava.objects;

public class GuitarExecution {

    // create a guitar class and a guitar execution
    // create 2 variables in guitar class
    // setup the variables through 2 diff constructors
    // create 2 method which prints both the variables individually
    // call the methods in the guitar execution class using 2 constructor


    public static void main(String[] args) {
        Guitar guitar = new Guitar(6);
        guitar.printStringOfGuitar();

        Guitar guitar1 = new Guitar(12, "Jackson");
        guitar1.printStringOfGuitar();
        guitar1.printBrandOfGuitar();

        System.out.println("********");
        guitar.printAll();

        System.out.println("********");
        guitar1.printAll();

    }
}
