package corejava.objects;

public class Laptop {


    // method calling from static context

    //static - keyword
    // main -- always static and void
    // static goes to anywhere (another static or another non static)
    // non-static method goes to non-static method (doesn't goes to another static method)

    public static void main(String[] args) {
        printMyName();
        // printMyAge();
    }

    // static
    public static void printMyName() {
        System.out.println("Sam");
    }

    // non-static method
    public void printMyAge() {
        System.out.println(27);
    }


    public void printMySub() {
        printMyAge();
        printMySub();
    }
}
