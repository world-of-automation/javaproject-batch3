package corejava.objects;

public class Siam {

    // create a default constructor
    // create a main method
    // create a static-void method and call into the main method
    // create a non-static-void method and call into the main method
    // create a static-return type of String method and call into the main method
    // create a non-static-return type of int method and call into the main method

    public Siam() {

    }

    public static void main(String[] args) {
        printMyAge();
        Siam siam = new Siam();
        siam.printMyName();
        String car = getMyCar();
        System.out.println(car);
        int year = siam.getMyBirthYear();
        System.out.println(year);
    }

    // access_specifier (static) void/return_data_type methodName(){}

    public static void printMyAge() {
        System.out.println(27);
    }

    public static String getMyCar() {
        return "Honda";
    }

    public void printMyName() {
        System.out.println("my name is siam");
    }

    public int getMyBirthYear() {
        return 1994;
    }


}