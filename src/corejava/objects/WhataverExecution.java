package corejava.objects;

public class WhataverExecution {

    public static void main(String[] args) {
        Whatever whatever = new Whatever();
        whatever.printYear();

        Whatever whatever2 = new Whatever(2020);
        whatever2.printYear();

        whatever.printYear();

        Whatever whatever3 = new Whatever("Shakir");
        whatever3.printName();
        whatever3.printYear();
        whatever2.printYear();

        Whatever whatever4 = new Whatever(3232);
        whatever4.printYear();
    }
}
