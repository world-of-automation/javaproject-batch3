package corejava.objects;

public class Whatever {

    int year;
    String name;

    //default
    public Whatever() {

    }

    //parameterized constructor
    public Whatever(int whateverYouWantToSetup) {
        this.year = whateverYouWantToSetup;
    }

    public Whatever(String whateverYouWantToSetup) {
        this.name = whateverYouWantToSetup;
    }

    public void printYear() {
        System.out.println(year);
    }

    public void printName() {
        System.out.println(name);
    }


}
