package corejava.oop.abstraction;

public abstract class Car {

    public void brake() {
        System.out.println("car has a  brake in order to stop");
    }

    public abstract void wheels();
}
