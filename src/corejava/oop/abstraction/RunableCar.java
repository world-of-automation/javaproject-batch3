package corejava.oop.abstraction;

// i - i --> interface-implement
//abstract class/ regular class - extends
// extend first , then implement
public class RunableCar extends Car implements Vehicle {

    // method overriding
    // -- 2 methods, same params, in 2 different classes
    // dynamic polymorphism/ runtime polymorphism

    @Override
    public void wheels() {
        System.out.println("4 wheels");
    }

    @Override
    public void move() {
        System.out.println("runable car can move");
    }

    @Override
    public void start() {
        System.out.println("runable car can start");
    }

    @Override
    public void stop() {
        System.out.println("runable car can stop");
    }

    @Override
    public void name() {
        System.out.println("runable car has a name : Ford");
    }
}
