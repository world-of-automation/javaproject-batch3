package corejava.oop.abstraction;

public class TestCar {
    public static void main(String[] args) {

        RunableCar runableCar = new RunableCar();
        // interface
        runableCar.move();
        runableCar.stop();
        runableCar.start();
        runableCar.name();

        //abstract
        runableCar.wheels();

        runableCar.brake();
    }
}
