package corejava.oop.abstraction;

public interface Vehicle {
    // concepts
    void move();

    void start();

    void stop();

    void name();

}
