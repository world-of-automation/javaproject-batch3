package corejava.oop.abstraction2;

public interface ConceptOfTheBusiness extends TestInterface1, TestInterface2 {

    // create a businessConcept (Interface) and have a idea
    // create a solid business company idea (Abstract) and have 1 idea and 1 full method
    // create a business and call the previous methods
    // create a test class and run the methods from the business


    // interface doesn't has a constructor thats why can't create obj
    // interface can be extended with other INTERFACE'S'
    // class can't have multiple parent but interface can have
    // interface all the methods are public

    void sell();

    void office();

    void employees();

    int revenue();
}
