package corejava.oop.abstraction2;

public class ITRecruiting extends RecruitingCompany implements ConceptOfTheBusiness {

    @Override
    public void sell() {
        System.out.println("we are selling from ITRecriuting");
    }

    @Override
    public void office() {
        System.out.println("we have a office from ITRecriuting");
    }

    @Override
    public void employees() {
        System.out.println("we have employees from ITRecriuting");
    }

    @Override
    public int revenue() {
        return 100;
    }


    @Override
    public void businessName() {
        System.out.println("whatever IT Company from ITRecriuting");
    }
}
