package corejava.oop.abstraction2;

public class ITRecruitingForBBC extends ITRecruiting {

    public void sell() {
        System.out.println("we are selling for BBC News");
    }

    public void tv() {
        System.out.println("tv from BBC News");
    }

}
