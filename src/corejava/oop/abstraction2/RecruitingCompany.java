package corejava.oop.abstraction2;

public abstract class RecruitingCompany extends TestAbstract1 implements TestInterface2, TestInterface1 {

    // can have constructor but still you can't cretae a obj
    // abstract methods must be mentioned with the keyword abstract
    // we can extend abstract class but to only 1
    // we can implement multiple interfaces to abstract class

    public RecruitingCompany() {

    }


    public void getRecruiters() {
        System.out.println("recruiters from abstract class");
    }

    public abstract void businessName();

}
