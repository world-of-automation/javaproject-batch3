package corejava.oop.abstraction2;

public class TestRecruiting {

    public static void main(String[] args) {
        ITRecruiting itRecruiting = new ITRecruiting();
        itRecruiting.businessName();
        itRecruiting.employees();
        itRecruiting.sell();

        ITRecruitingForBBC itRecruitingForBBC = new ITRecruitingForBBC();
        itRecruitingForBBC.office();
        itRecruitingForBBC.sell();
        itRecruitingForBBC.tv();
    }
}
