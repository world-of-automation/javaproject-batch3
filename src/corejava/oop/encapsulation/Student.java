package corejava.oop.encapsulation;

public class Student {

    // encapsulation  --> data hiding process --> setter/ getter
    // abstraction
    // inheritance

    private int id;
    private String name;
    private String dob;
    private String college;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
