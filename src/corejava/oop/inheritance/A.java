package corejava.oop.inheritance;

// A - grand parent class for all
public class A {

    public void methodFromClassA() {
        System.out.println("method details from class A");
    }
}
