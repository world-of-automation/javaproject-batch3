package corejava.oop.inheritance;

// B - parent class for C
// B - child class for A
public class B extends A {

    // when its a parent class

    public static void main(String[] args) {
        B b = new B();
        b.methodFromClassB();
        b.methodFromClassA();
    }

    public void methodFromClassB() {
        System.out.println("method details from class B");
    }
}
