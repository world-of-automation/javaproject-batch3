package corejava.oop.inheritance;

// grand child for A
// child for B
public class C extends B {

    public static void main(String[] args) {
        C c = new C();
        c.methodFromClassC();
        c.methodFromClassB();
        c.methodFromClassA();

    }

    public void methodFromClassC() {
        System.out.println("method details from class C");
    }
}
