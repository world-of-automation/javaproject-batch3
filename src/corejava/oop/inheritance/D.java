package corejava.oop.inheritance;

public class D extends A {

    // a class could be parent for multiple classes
    // but a class can not be the child of multiple , bcz Jva doesn't allow multiple inheritance
}
