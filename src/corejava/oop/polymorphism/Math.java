package corejava.oop.polymorphism;

public class Math {
    // polymorphism --> having many forms

    // method overloading
    // -- same method name, diff params, methods are in same class
    // static polymorphism/ compile time polymorphism
    public void calculator(int a, int b) {
        System.out.println(a + b);
    }

    public void calculator(int a, int b, int c) {
        System.out.println(a + b - c);
    }

    public void calculator(String data) {
        System.out.println(data);
    }


    // method overriding
    // -- see examples at abstraction.RunableCar.java


}
