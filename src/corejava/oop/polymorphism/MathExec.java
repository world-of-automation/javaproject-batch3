package corejava.oop.polymorphism;

public class MathExec {

    public static void main(String[] args) {
        Math math = new Math();
        math.calculator(10, 20);
        math.calculator(10, 20, 20);
        Math math2 = new Math();
        math2.calculator("whatever");
    }
}
