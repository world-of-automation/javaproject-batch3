package corejava.singleton;

public class Laptop {

    //1.private static object of the class (half way)
    private static Laptop laptop = null;

    //2. private constructor
    private Laptop() {

    }

    //3. a public static method which has return type of the class(object)
    public static Laptop getInstance() {
        if (laptop == null) {
            laptop = new Laptop();
        }
        return laptop;
    }


    public String getModelOfTheLaptop() {
        return "MacBook Pro";
    }


}
