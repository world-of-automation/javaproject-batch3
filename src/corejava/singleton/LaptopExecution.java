package corejava.singleton;

public class LaptopExecution {

    public static void main(String[] args) {

        Laptop laptop = Laptop.getInstance();
        Laptop laptop4 = Laptop.getInstance();

        String modelName = laptop.getModelOfTheLaptop();
        System.out.println(modelName);

        String modelName4 = laptop4.getModelOfTheLaptop();
        System.out.println(modelName4);
    }
}
